import reviewsSlice from "../slices/reviewsSlice";

export const {
  createReviewsRequest,
  createReviewsSuccess,
  createReviewsFailure,
  fetchReviewsRequest,
  fetchReviewsSuccess,
  fetchReviewsFailure,
  deleteReviewRequest,
  deleteReviewSuccess,
  deleteReviewFailure,
} = reviewsSlice.actions;
