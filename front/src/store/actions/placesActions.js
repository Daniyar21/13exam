import placesSlice from "../slices/placesSlice";

export const {
  createPlaceRequest,
  createPlaceSuccess,
  createPlaceFailure,
  fetchPlacesFailure,
  fetchPlacesRequest,
  fetchPlacesSuccess,
  fetchSoloPlaceRequest,
  fetchSoloPlaceSuccess,
  fetchSoloPlaceFailure,
  deletePlaceFailure,
  deletePlaceSuccess,
  deletePlaceRequest,
} = placesSlice.actions;
