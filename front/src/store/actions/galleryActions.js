import gallerySlice from "../slices/gallerySlice";

export const {
  createGalleryFailure,
  createGalleryRequest,
  createGallerySuccess,
  fetchGalleryFailure,
  fetchGalleryRequest,
  fetchGallerySuccess,
  deleteGalleryFailure,
  deleteGalleryRequest,
  deleteGallerySuccess,
} = gallerySlice.actions;
