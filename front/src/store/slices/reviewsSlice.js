import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  reviews: [],
  loadingReviews: false,
  errorReviews: null,
};

const name = "reviews";

const reviewsSlice = createSlice({
  name,
  initialState,
  reducers: {
    createReviewsRequest: (state) => {
      state.loadingReviews = true;
    },
    createReviewsSuccess: (state) => {
      state.loadingReviews = false;
    },
    createReviewsFailure: (state, { payload: error }) => {
      state.loadingReviews = false;
      state.errorReviews = error;
    },
    fetchReviewsRequest: (state) => {
      state.loadingReviews = true;
    },
    fetchReviewsSuccess: (state, { payload: reviews }) => {
      state.loadingReviews = false;
      state.reviews = reviews;
    },
    fetchReviewsFailure: (state, { payload: error }) => {
      state.loadingReviews = false;
      state.errorReviews = error;
    },
    deleteReviewRequest: (state) => {
      state.loadingReviews = true;
    },
    deleteReviewSuccess: (state) => {
      state.loadingReviews = false;
    },
    deleteReviewFailure: (state, { payload: error }) => {
      state.loadingPlace = false;
      state.errorReviews = error;
    },
  },
});

export default reviewsSlice;
