import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  places: [],
  place: null,
  loadingPlace: false,
  errorPlace: null,
};

const name = "places";

const placesSlice = createSlice({
  name,
  initialState,
  reducers: {
    createPlaceRequest: (state) => {
      state.loadingPlace = true;
    },
    createPlaceSuccess: (state) => {
      state.loadingPlace = false;
    },
    createPlaceFailure: (state, { payload: error }) => {
      state.loadingPlace = false;
      state.errorPlace = error;
    },
    fetchPlacesRequest: (state) => {
      state.loadingPlace = true;
    },
    fetchPlacesSuccess: (state, { payload: places }) => {
      state.loadingPlace = false;
      state.places = places;
    },
    fetchPlacesFailure: (state, { payload: error }) => {
      state.loadingPlace = false;
      state.errorPlace = error;
    },
    fetchSoloPlaceRequest: (state) => {
      state.loadingPlace = true;
    },
    fetchSoloPlaceSuccess: (state, { payload: place }) => {
      state.loadingPlace = false;
      state.place = place;
    },
    fetchSoloPlaceFailure: (state, { payload: error }) => {
      state.loadingPlace = false;
      state.errorPlace = error;
    },
    deletePlaceRequest: (state) => {
      state.loadingPlace = true;
    },
    deletePlaceSuccess: (state) => {
      state.loadingPlace = false;
    },
    deletePlaceFailure: (state, { payload: error }) => {
      state.loadingPlace = false;
      state.errorPlace = error;
    },
  },
});

export default placesSlice;
