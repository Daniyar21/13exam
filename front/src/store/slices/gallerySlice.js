import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  gallery: [],
  loadingGallery: false,
  errorGallery: null,
};

const name = "gallery";

const gallerySlice = createSlice({
  name,
  initialState,
  reducers: {
    createGalleryRequest: (state) => {
      state.loadingGallery = true;
    },
    createGallerySuccess: (state) => {
      state.loadingGallery = false;
    },
    createGalleryFailure: (state, { payload: error }) => {
      state.loadingGallery = false;
      state.errorGallery = error;
    },
    fetchGalleryRequest: (state) => {
      state.loadingGallery = true;
    },
    fetchGallerySuccess: (state, { payload: gallery }) => {
      state.loadingGallery = false;
      state.gallery = gallery;
    },
    fetchGalleryFailure: (state, { payload: error }) => {
      state.loadingGallery = false;
      state.errorGallery = error;
    },
    deleteGalleryRequest: (state) => {
      state.loadingGallery = true;
    },
    deleteGallerySuccess: (state) => {
      state.loadingGallery = false;
    },
    deleteGalleryFailure: (state, { payload: error }) => {
      state.loadingGallery = false;
      state.errorGallery = error;
    },
  },
});

export default gallerySlice;
