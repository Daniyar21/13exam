import { combineReducers } from "redux";
import notifierSlice from "./slices/notifierSlice";
import usersSlice from "./slices/usersSlice";
import placesSlice from "./slices/placesSlice";
import reviewsSlice from "./slices/reviewsSlice";
import gallerySlice from "./slices/gallerySlice";

const rootReducer = combineReducers({
  users: usersSlice.reducer,
  notifier: notifierSlice.reducer,
  places: placesSlice.reducer,
  reviews: reviewsSlice.reducer,
  gallery:gallerySlice.reducer
});

export default rootReducer;
