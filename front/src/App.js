import React from "react";
import Login from "./container/Login/Login";
import Register from "./container/Register/Register";
import { Redirect, Route, Switch } from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import AddNewPlaces from "./container/AddNewPlaces/AddNewPlaces";
import { useSelector } from "react-redux";
import AllPlaces from "./container/AllPlaces/AllPlaces";
import SoloPlaces from "./container/SoloPlaces/SoloPlaces";

const ProtectedRoute = ({ isAllowed, redirectTo, ...props }) => {
  return isAllowed ? <Route {...props} /> : <Redirect to={redirectTo} />;
};

const App = () => {
  const user = useSelector((state) => state.users.user);

  return (
    <Layout>
      <Switch>
        <Route path="/login" component={Login} />
        <Route path="/register" component={Register} />
        <ProtectedRoute path="/places/new" component={AddNewPlaces} isAllowed={user} redirectTo="/login" />
        <Route path="/" exact component={AllPlaces} />
        <Route path="/place/:id" component={SoloPlaces} />
      </Switch>
    </Layout>
  );
};

export default App;
