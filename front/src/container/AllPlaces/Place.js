import { Button, Card, CardHeader, CardMedia } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";
import { apiURL } from "../../config";
import { useDispatch, useSelector } from "react-redux";
import { deletePlaceRequest } from "../../store/actions/placesActions";

const Place = (props) => {
  const user = useSelector((state) => state.users.user);
  const dispatch = useDispatch();
  return (
    <Card>
      <Link to={"/place/" + props.id} style={{ color: "black", textDecoration: "none" }}>
        <CardMedia image={apiURL + "/" + props.image} style={{ width: "80%", height: "150px" }} />
        <CardHeader title={props.title} />
      </Link>
      {user && user.role === "admin" ? (
        <Button
          variant="contained"
          color="primary"
          style={{ margin: "5px" }}
          onClick={() => dispatch(deletePlaceRequest(props.id))}
        >
          delete
        </Button>
      ) : null}
    </Card>
  );
};

export default Place;
