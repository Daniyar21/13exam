import React from "react";
import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
import { fetchSoloPlaceRequest } from "../../store/actions/placesActions";
import { Button, Grid, Typography } from "@material-ui/core";
import { apiURL } from "../../config";
import ReviewsForm from "../../components/ReviewsForm/ReviewsForm";
import { deleteReviewRequest, fetchReviewsRequest } from "../../store/actions/reviewsActions";
import MeRating from "../../components/MeRating/MeRating";
import moment from "moment";
import AverageRating from "../../components/AverageRating/AverageRating";
import GalleryForm from "../../components/GalleryForm/GalleryForm";
import Gallery from '../../components/Gallery/Gallery';

const SoloPLaces = (props) => {
  const dispatch = useDispatch();
  const id = props.match.params.id;
  const place = useSelector((state) => state.places.place);
  const reviews = useSelector((state) => state.reviews.reviews);
  const user = useSelector((state) => state.users.user);

  useEffect(() => {
    dispatch(fetchSoloPlaceRequest(id));
    dispatch(fetchReviewsRequest(id));
  }, [dispatch, id]);

  return (
    <Grid container spacing={2} direction="column">
      {place && (
        <>
          <Grid item>
            <Typography variant="h5">{place.title}</Typography>
          </Grid>
          <Grid item container justifyContent="space-between" wrap="nowrap">
            <Grid item style={{ width: "50%" }}>
              <Typography variant="h6">{place.description}</Typography>
            </Grid>
            <Grid item style={{ width: "50%" }}>
              <img
                src={apiURL + "/" + place.image}
                alt={place.image}
                style={{ width: "500px", height: "300px" }}
              />
            </Grid>
          </Grid>
          <Grid item>
                <Gallery place={id}/>
          </Grid>
          <Grid item>
            <AverageRating reviews={reviews} />
          </Grid>
          <Grid item container direction="column" spacing={2}>
            <Typography variant="h6">Reviews</Typography>
            {reviews.map((review) => (
              <Grid item key={review._id} style={{ border: "1px solid black", margin: "2px" }}>
                <Grid item>
                  <Typography>
                    {moment(review.datetime).format("MMMM Do YYYY, h:mm:ss a")},{review.user.displayName} said
                  </Typography>
                </Grid>
                <Grid item>
                  <Typography variant="h6">{review.text}</Typography>
                </Grid>
                <Grid item container justifyContent="flex-start">
                  <Grid item>
                    <Typography>Quality Of Food</Typography>
                    <MeRating readOnly value={review.qualityOfFood} disabled />
                  </Grid>
                  <Grid item>
                    <Typography>Service Quality</Typography>
                    <MeRating readOnly value={review.serviceQuality} disabled />
                  </Grid>

                  <Grid item>
                    <Typography>Interior</Typography>
                    <MeRating readOnly value={review.interior} disabled />
                  </Grid>
                </Grid>
                {user && user.role === "admin" ? (
                  <Button
                    variant="contained"
                    color="secondary"
                    onClick={() => dispatch(deleteReviewRequest({ reviewId: review._id, placeId: id }))}
                  >
                    Delete
                  </Button>
                ) : null}
              </Grid>
            ))}
          </Grid>
          <Grid item>
            {user && user.role === 'user' && (
            <ReviewsForm place={id} />
                )}
          </Grid>
          <Grid item>
            {user && user.role === 'user' && (
            <GalleryForm place={id}/>
                )}
          </Grid>
        </>
      )}
    </Grid>
  );
};

export default SoloPLaces;
