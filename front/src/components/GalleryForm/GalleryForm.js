import React, { useState } from "react";
import { Button, Grid } from "@material-ui/core";
import FileInput from "../UI/Form/FileInput";
import { useDispatch } from "react-redux";
import { createGalleryRequest } from "../../store/actions/galleryActions";

const GalleryForm = ({ place }) => {
  const dispatch = useDispatch();
  const [image, setImage] = useState("");

  const fileChangeHandler = (e) => {
    setImage(e.target.files[0]);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append("image", image);
    formData.append("place", place);

    dispatch(createGalleryRequest({ formData, place }));
  };

  return (
    <Grid item container direction="column" spacing={2} component={"form"} onSubmit={onSubmit}>
      <Grid item>
        Choose File
        <FileInput onChange={fileChangeHandler} label="Image" />
      </Grid>
      <Grid item>
        <Button variant="contained" color="primary" type="submit">
          Upload
        </Button>
      </Grid>
    </Grid>
  );
};

export default GalleryForm;
