import React, { useState } from "react";
import { Button, Grid, Typography } from "@material-ui/core";
import FormElement from "../UI/Form/FormElement";
import MeRating from "../MeRating/MeRating";
import { useDispatch } from "react-redux";
import { createReviewsRequest } from "../../store/actions/reviewsActions";

const ReviewsForm = ({ place }) => {
  const dispatch = useDispatch();
  const [review, setReview] = useState({
    text: "",
    qualityOfFood: 0,
    serviceQuality: 0,
    interior: 0,
  });

  const onChangeRating = (e) => {
    const { name, value } = e.target;

    setReview((prev) => ({ ...prev, [name]: Number(value) }));
  };

  const onSubmit = (e) => {
    e.preventDefault();

    const reviewData = { ...review, place };

    dispatch(createReviewsRequest(reviewData));
  };

  return (
    <Grid
      item
      container
      direction="column"
      spacing={2}
      component="form"
      onSubmit={onSubmit}
    >
      <Grid item>
        <Typography variant="h6">Add review</Typography>
        <FormElement
          onChange={(e) => setReview((prev) => ({ ...prev, text: e.target.value }))}
          name="text"
          value={review.text}
        />
      </Grid>
      <Grid item container direction="column" spacing={4}>
        <Grid item container>
          <Grid item>
            <Typography component="legend">Quality Of Food</Typography>
            <MeRating value={review.qualityOfFood} name="qualityOfFood" onChange={onChangeRating} />
          </Grid>
          <Grid item>
            <Typography component="legend">Service Quality</Typography>
            <MeRating value={review.serviceQuality} name="serviceQuality" onChange={onChangeRating} />
          </Grid>
          <Grid item>
            <Typography component="legend">Interior</Typography>
            <MeRating value={review.interior} name="interior" onChange={onChangeRating} />
          </Grid>
        </Grid>
        <Grid item>
          <Button type="submit" color="primary" variant="contained">
            Submit Review
          </Button>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default ReviewsForm;
