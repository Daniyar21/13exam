import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Rating from "@material-ui/lab/Rating";

const useStyles = makeStyles({
  root: {
    width: 200,
    display: "flex",
    alignItems: "center",
  },
});

const MeRating = ({ onChange, name, value, disabled, ...props }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Rating precision={0.5} name={name} value={value} onChange={onChange} disabled={disabled} {...props} />
    </div>
  );
};

export default MeRating;
