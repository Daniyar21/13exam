import React, { useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import ImageList from "@material-ui/core/ImageList";
import ImageListItem from "@material-ui/core/ImageListItem";
import { useDispatch, useSelector } from "react-redux";
import { deleteGalleryRequest, fetchGalleryRequest } from "../../store/actions/galleryActions";
import { apiURL } from "../../config";
import { IconButton, ImageListItemBar } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "space-around",
    overflow: "hidden",
    backgroundColor: theme.palette.background.paper,
  },
  imageList: {
    flexWrap: "nowrap",
    transform: "translateZ(0)",
  },
  title: {
    color: theme.palette.primary.light,
  },
  titleBar: {
    background: "linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)",
  },
}));

const Gallery = ({ place }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const gallery = useSelector((state) => state.gallery.gallery);
  const user = useSelector((state) => state.users.user);

  useEffect(() => {
    dispatch(fetchGalleryRequest(place));
  }, [dispatch, place]);

  return (
    <div className={classes.root}>
      <ImageList className={classes.imageList} cols={3}>
        {gallery.map((item) => (
          <ImageListItem key={item._id}>
            <img src={apiURL + "/" + item.image} alt={item._id} style={{ width: "100%" }} />
            <ImageListItemBar
              title={item.title}
              classes={{
                root: classes.titleBar,
                title: classes.title,
              }}
              actionIcon={
                user &&
                user.role === "admin" && (
                  <IconButton
                    onClick={() => dispatch(deleteGalleryRequest({ placeId: place, galleryId: item._id }))}
                  >
                    <DeleteIcon className={classes.title} />
                  </IconButton>
                )
              }
            />
          </ImageListItem>
        ))}
      </ImageList>
    </div>
  );
};

export default Gallery;
