const mongoose = require("mongoose");
const config = require("./config");
const { nanoid } = require("nanoid");
const User = require("./models/User");
const Place = require("./models/Place");
const Gallery = require("./models/Gallery");

const run = async () => {
  await mongoose.connect(config.db.url, config.db.options);

  const collections = await mongoose.connection.db.listCollections().toArray();

  for (const coll of collections) {
    await mongoose.connection.db.dropCollection(coll.name);
  }

  const [user, user2, admin] = await User.create(
    {
      email: "user@gmail.com",
      password: "12345",
      token: nanoid(),
      role: "user",
      displayName: "User",
      avatar: "fixtures/user.png",
    },
    {
      email: "admin@gmail.com",
      password: "12345",
      token: nanoid(),
      role: "admin",
      displayName: "Admin",
      avatar: "fixtures/admin.png",
    },
    {
      email: "user2@gmail.com",
      password: "12345",
      token: nanoid(),
      role: "user",
      displayName: "User2",
      avatar: "fixtures/user.png",
    }
  );

  const [place1, place2, place3, place4] = await Place.create(
    {
      id: nanoid(),
      user: user,
      title: "Arzu",
      description:"Great pizza, mozzarella sticks, calamari, chicken tenders, I could go on.",
      image: "fixtures/res_1.jpg",
    },
    {
      id: nanoid(),
      user: user,
      title: "Asman",
      description:"Def recommend their cheese pizza too, really good sauce and crust",
      image: "fixtures/res_2.jpg",
    },
    {
      id: nanoid(),
      user: user2,
      title: "Diar",
      description:"Friendly staff and probably the best cheese pizza I’ve had!",
      image: "fixtures/res_3.jpg",
    },
    {
      id: nanoid(),
      user: user2,
      title: "Besh Barmak",
      description:"If we (I) are (am) lucky, we go for dinner one day and breakfast another.",
      image: "fixtures/res_5.jpg",
    }
  );

  await Gallery.create(
    {
      place: place1,
      image: "fixtures/1.jpg",
      user:user,
    },
    {
      place: place1,
      image: "fixtures/2.jpg",
      user:user2,
    },
    {
      place: place2,
      image: "fixtures/3.jpg",
      user:user2,
    },
    {
      place: place3,
      image: "fixtures/4.jpg",
      user:user,
    },
    {
      place: place3,
      image: "fixtures/1.jpg",
      user:user,
    },
    {
      place: place4,
      image: "fixtures/2.jpg",
      user:user,
    },
    {
      place: place4,
      image: "fixtures/3.jpg",
      user:user,
    },

  );

  await mongoose.connection.close();
};

run().catch((e) => {
  console.error(e);
  process.exit(1);
});
