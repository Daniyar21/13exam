const mongoose = require("mongoose");

const ReviewSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  place: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Place",
    required: true,
  },
  text: {
    type: String,
    required: true,
  },
  datetime: String,
  qualityOfFood: {
    type: Number,
    min: 0,
    max: 5,
    required: true,
  },
  serviceQuality: {
    type: Number,
    min: 0,
    max: 5,
    required: true,
  },
  interior: {
    type: Number,
    min: 0,
    max: 5,
    required: true,
  },
});

const Review = mongoose.model("Review", ReviewSchema);

module.exports = Review;
