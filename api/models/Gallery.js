const mongoose = require("mongoose");

const GallerySchema = new mongoose.Schema({
  place: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Place",
    required: true,
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "User",
    required: true,
  },
  image: {
    type: String,
    required: true,
  },
});

const Gallery = mongoose.model("Gallery", GallerySchema);

module.exports = Gallery;
