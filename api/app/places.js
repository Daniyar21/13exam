const express = require("express");
const Place = require("../models/Place");
const upload = require("../multer").places;
const router = express.Router();
const auth = require("../middleware/auth.js");
const permit = require("../middleware/permit");

router.get("/", async (req, res) => {
  try {
    const places = await Place.find();
    return res.send(places);
  } catch (e) {
    return res.sendStatus(400);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const place = await Place.findOne({ _id: req.params.id }).populate("user");
    return res.send(place);
  } catch (e) {
    return res.sendStatus(400);
  }
});

router.delete("/:id", auth, permit("admin"), async (req, res) => {
  try {
    await Place.deleteOne({ _id: req.params.id });
    return res.send("Your Place deleted");
  } catch (e) {
    return res.sendStatus(400);
  }
});

router.post("/", auth, upload.single("image"), async (req, res) => {
  try {
    if (JSON.parse(req.body.checked)) {
      const place = await new Place({
        user: req.user,
        title: req.body.title,
        description: req.body.description,
        image: 'uploads/'+req.file.filename,

      });
      await place.save();
      return res.send(place);
    } else {
      return res.sendStatus(400);
    }
  } catch (e) {
    return res.sendStatus(400);
  }
});

module.exports = router;
